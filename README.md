# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Inside VisualStudio pres f5
* On loading will run migrations and setup Db
* In VSCode run the angular application.

# README #

This README would normally document whatever steps are necessary to get your application up and running.

The problem and solution was created for the BOOKING PLATFORM Challenge. The solution is to allow the user to book rooms that are listed on the website.
The rooms could only be booked if they are not already booked out for the the desired period or if the number of occupents exceeded the rooms 
capacity.

This solution was forcused on a full stack solution.

The reasoning behind the technical choices I made was due to this project was more logically to dissect.

I used .net Core and Angular to build this chellenge because it allows a great solution for client server applications.
I used Entity framework and SQL for DB and database migrations. The application will run migrations on startup.
Used rep pattern with interfaces so can mock data for unit tests.
Entity framework was used in the backend for admin/user JWT,claims and roles for Auhtentication.
I used mvc and a repo pattern for entity framework.
The backend, on starting will load up Swagger.
Swagger is used to test backend functionailty as well as documentation for the backend.

Trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the project.
Due to the size of this project, I did not get a complete solution finished. Given more time I would have designed the front end with UX in mind.
The Angular UI is very basic with a page of available rooms show on load.
Selecting to book a room displays a form to use to enter details. This conponent is not wired up to the back end.
Login navigation opens a modal for login/registration. This is not wired up to the backend.
This was a great project because I have had little experience with Angular front end and connecting to the back end. 
I had to add extra focus here which was a trade-off for getting the application more complete.
The backend is fully functioning. Contains methods to handle rooms, bookings and authentication.

ToDo given more time:
Connect the components and modals to send the data to the controllers on the backend.
Send roomId to the reservation form and populate the appropiate row with this data.
Implemnet the Admin crud in Angular.
Keep track of a session so that only relative infopmation and functionality is presented.
Method to add images and get images for a room. Populate the Slideshow with these images.

Link to other code you're particularly proud of.
I do not have any that is not work related.

Link to your resume or public profile.
https://www.linkedin.com/feed/

Link to to the hosted application where applicable.
https://bitbucket.org/kevincrook/gobooking/src/master/