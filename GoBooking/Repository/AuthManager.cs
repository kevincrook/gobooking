﻿using GoBooking.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoBooking.Repository
{
    public class AuthManager : IAuthRepository<IdentityUser>
    {
        readonly AuthDbContext _authDbContext;

        public void AddToRole(long id)
        {
            throw new NotImplementedException();
        }

        public void CreateUser(IdentityUser entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(IdentityUser entity)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(IdentityUser dbUser, IdentityUser user)
        {
            dbUser.Id = user.Id;
            dbUser.UserName = user.UserName;
            dbUser.PasswordHash = user.PasswordHash;

            _authDbContext.SaveChanges();
        }

        IEnumerable<IdentityUser> IAuthRepository<IdentityUser>.GetAllUsers()
        {
            return _authDbContext.IdentityUser.ToList();
        }

        IdentityUser IAuthRepository<IdentityUser>.GetUser(long id)
        {
            return _authDbContext.IdentityUser
                .FirstOrDefault(x => x.Id == id.ToString());
        }
    }
}
