﻿using GoBooking.Interfaces;
using GoBooking.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoBooking.Repository
{
    public class BookingManager : IDataRepository<Reservation>
    {
        private readonly BookingContext _bookingContext;
        private readonly ILogger _logger;
        public string Message { get; set; }
        public BookingManager(ILogger<Reservation> logger, BookingContext context)
        {
            _bookingContext = context;
            _logger = logger;
        }

        public async Task<Reservation> AddAsync (Reservation res)
        {
            var currentBookings = FindAll()
                .Where(b => b.RoomID == res.RoomID)
                .Select(b => (res.DateBookedTo < b.DateBookedTo && res.DateBookedFrom > b.DateBookedFrom))
                .FirstOrDefault();

            if (currentBookings)
            {
                Message = $"Reservation has already been booked";
                _logger.LogInformation(Message);
                return null;
            }

            _bookingContext.Reservations.Add(res);
            _bookingContext.SaveChanges();

            return res;
        }

        public async Task<Reservation> DeleteAsync(Reservation res)
        {
            try
            {
                _bookingContext.Reservations.Remove(res);
                _bookingContext.SaveChanges();
                return res;

            }
            catch (Exception)
            {
                throw new Exception($"{nameof(res)} could not be deleted");
            }
        }

        public Reservation FindById(long id)
        {
            return _bookingContext.Reservations
                .FirstOrDefault(x => x.BookingID == id);
        }

        public IEnumerable<Reservation> FindAll()
        {
            return _bookingContext.Reservations.ToList();

        }

        public async Task<Reservation> UpdateAsync(Reservation dbRes, Reservation res)
        {
            try
            {
                dbRes.BookingID = res.BookingID;
                dbRes.RoomID = res.RoomID;
                dbRes.DateBookedFrom = res.DateBookedFrom;
                dbRes.DateBookedTo = res.DateBookedTo;
                dbRes.Email = res.Email;
                dbRes.Occupants = res.Occupants;

                _bookingContext.SaveChanges();

                return res;
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(res)} could not be updated");
            }
        }
    }
}