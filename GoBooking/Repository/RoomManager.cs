﻿using GoBooking.Interfaces;
using GoBooking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoBooking.Repository
{
    public class RoomManager : IDataRepository<Room>
    {
        readonly BookingContext _roomContext;
        public RoomManager(BookingContext context)
            : base()
        {
            _roomContext = context;
        }
        public async Task<Room> AddAsync(Room entity)
        {
            try
            {
                _roomContext.Room.Add(entity);
                _roomContext.SaveChanges();
                return entity;
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(entity)} could not be saved");
            }
        }

        public async Task<Room> DeleteAsync(Room entity)
        {
            try
            {
                _roomContext.Room.Remove(entity);
                _roomContext.SaveChanges();
                return entity;
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(entity)} could not be deleted");
            }
        }

        public Room FindById(long id)
        {
            return _roomContext.Room
                .FirstOrDefault(x => x.RoomID == id);
        }

        public IEnumerable<Room> FindAll()
        {
            return _roomContext.Room.ToList();
        }

        public async Task<Room> UpdateAsync(Room dbEntity, Room entity)
        {
            try
            {
                dbEntity.RoomID = entity.RoomID;
                dbEntity.RoomTitle = entity.RoomTitle;
                dbEntity.Description = entity.Description;
                dbEntity.Address = entity.Address;
                dbEntity.Capacity = entity.Capacity;
                dbEntity.Images = entity.Images;

                _roomContext.SaveChanges();
                return dbEntity;
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(entity)} could not be updated");
            }
        }
    }
}
