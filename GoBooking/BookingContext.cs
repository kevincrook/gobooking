﻿using GoBooking.Models;
using Microsoft.EntityFrameworkCore;

namespace GoBooking
{
    public class BookingContext : DbContext
    {
        public BookingContext(DbContextOptions<BookingContext> options)
            : base(options)
        {
        }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Room> Room { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Room>().HasData(new Room
            {
                RoomID = 1,
                RoomTitle = "Lavender Room",
                Description = "Well lite room with ocean views",
                Address = "21 Ocean st Marrochydore",
                Capacity = 5,
                RoomPrice = 290

            }, new Room
            {
                RoomID = 2,
                RoomTitle = "Oval Room",
                Description = "Spacious room for all your parliamentary needs",
                Address = "21 Ocean st Marrochydore",
                Capacity = 15,
                RoomPrice = 159
            },
            new Room
            {
                RoomID = 3,
                RoomTitle = "Beach Front Room",
                Description = "Well lite room with ocean views",
                Address = "21 Ocean st Marrochydore",
                Capacity = 5,
                RoomPrice = 490

            },
            new Room
            {
                RoomID = 4,
                RoomTitle = "Mountain View Room",
                Description = "Well lite room with ocean views",
                Address = "21 Ocean st Marrochydore",
                Capacity = 5,
                RoomPrice = 390

            },
            new Room
            {
                RoomID = 5,
                RoomTitle = "Dark Room",
                Description = "Not a Well lite room with no views",
                Address = "21 Ocean st Marrochydore",
                Capacity = 1,
                RoomPrice = 90

            },
            new Room
            {
                RoomID = 6,
                RoomTitle = "Challenge Room",
                Description = "Metally challeging room designed to agrivate you sensless",
                Address = "21 Ocean st Marrochydore",
                Capacity = 5,
                RoomPrice = 290

            }
            );

            modelBuilder.Entity<Image>().HasData(new Image
            {
                ImageID = 1,
                RoomId = 1,
                FileName = "lanenderRoom.png"

            }, new Image
            {
                ImageID = 2,
                RoomId = 2,
                FileName = "ovalRomm.png"

            });
        }
    }
}
