﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GoBooking.Models;
using Microsoft.Extensions.Logging;
using GoBooking.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace GoBooking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomsManagerController : ControllerBase
    {
        private readonly IDataRepository<Room> _context;
        private readonly ILogger _logger;
        public string Message { get; set; }

        /// <summary>
        /// Construcor that takes in the logger and Interface.
        /// </summary>
        public RoomsManagerController(ILogger<Room> logger, IDataRepository<Room> context)
        {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Get request to return a list of all rooms.
        /// </summary>
        // GET: api/RoomsManager
        [HttpGet]
        public IEnumerable<Room> GetAllRooms()
        {
            return _context.FindAll();
        }

        /// <summary>
        /// Get request to return a single room via roomId.
        /// </summary>
        // GET: api/RoomsManager/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Room>> GetRoomByID(int id)
        {
            var room = _context.FindById(id);

            if (room == null)
            {
                Message = $"Room is not found";
                _logger.LogInformation(Message);
                return NotFound(Message);
            }

            return room;
        }

        /// <summary>
        /// Put request to update a rooms properties.
        /// Requires Autherization.
        /// </summary>
        // PUT: api/RoomsManager/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = "Manager,Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSelectedRoom(int id, Room room)
        {
            if (id != room.RoomID)
            {
                Message = $"Room not found";
                _logger.LogInformation(Message);
                return BadRequest(Message);
            }

            var dbRoom = _context.FindById(id);

            try
            {
                await _context.UpdateAsync(dbRoom, room);
            }
            catch (DbUpdateConcurrencyException)
            {
                Message = $"Room does not exist";
                _logger.LogInformation(Message);
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Post request to create a room.
        ///Requires Autherization.
        /// </summary>
        // POST: api/RoomsManager
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = "Manager,Admin")]
        [HttpPost]
        public async Task<ActionResult<Room>> CreateARoom(Room room)
        {
            await _context.AddAsync(room);

            return CreatedAtAction("GetRoom", new { id = room.RoomID }, room);
        }

        /// <summary>
        /// Delete request to remove a room by Id.
        /// Requires Autherization.
        /// </summary>
        // DELETE: api/RoomsManager/5
        [Authorize(Roles = "Manager,Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult<Room>> DeleteSelectedRoom(int id)
        {
            var room = _context.FindById(id);
            if (room == null)
            {
                Message = $"Room does not exist";
                _logger.LogInformation(Message);
                return NotFound(Message);
            }

            await _context.DeleteAsync(room);

            return room;
        }
    }
}
