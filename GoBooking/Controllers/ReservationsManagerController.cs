﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GoBooking.Models;
using Microsoft.Extensions.Logging;
using GoBooking.Interfaces;

namespace GoBooking.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsManagerController : ControllerBase
    {
        private readonly IDataRepository<Reservation> _context;
        private readonly ILogger _logger;
        public string Message { get; set; }

        public ReservationsManagerController(ILogger<Reservation> logger, IDataRepository<Reservation> context)
        {
            _context = context;
            _logger = logger;
        }

        /// <summary>
        /// Returns a list of reservations.
        /// </summary>
        // GET: api/ReservationManager
        [HttpGet]
        public IEnumerable<Reservation> GetReservations()
        {
            return _context.FindAll();
        }

        /// <summary>
        /// Returns a single reservation by ID.
        /// </summary>
        // GET: api/ReservationManager/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Reservation>> GetReservation(int id)
        {
            var reservation = _context.FindById(id);

            if (reservation == null)
            {
                Message = $"Reservation is currently null as at  {DateTime.UtcNow.ToLongTimeString()}";
                _logger.LogInformation(Message);
                return NotFound();
            }

            return reservation;
        }

        /// <summary>
        /// Updates a single reservation.
        /// </summary>
        // PUT: api/ReservationManager/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReservation(int id, Reservation reservation)
        {
            if (id != reservation.BookingID)
            {
                Message = $"Reservation has no booking iD";
                _logger.LogInformation(Message);
                return BadRequest(Message);
            }

            var dBRes = _context.FindById(id);

            try
            {
                await _context.UpdateAsync(dBRes, reservation);
            }
            catch (DbUpdateConcurrencyException)
            {
                Message = $"Reservation does not exist";
                _logger.LogInformation(Message);
                return NotFound();

            }

            return NoContent();
        }

        /// <summary>
        /// Adds a reservation.
        /// Pass in reservation and checks that the dates dont over lap.
        /// </summary>
        // POST: api/ReservationManager
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Reservation>> PostReservation(Reservation reservation)
        {
            if (string.IsNullOrEmpty(reservation.Email))
            {
                Message = $"Email is reguired to make reservations";
                _logger.LogInformation(Message);
                return BadRequest(Message);
            }

            var makeRes = await _context.AddAsync(reservation);

            if (makeRes == null)
            {
                Message = $"Reservation has already been booked";
                _logger.LogInformation(Message);
                return NotFound(Message);
            }

            await _context.AddAsync(reservation);

            return CreatedAtAction("GetReservation", new { id = reservation.BookingID }, reservation);
        }

        /// <summary>
        /// Removes a single reservation.
        /// </summary>
        // DELETE: api/ReservationManager/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Reservation>> DeleteReservation(int id)
        {
            var reservation = _context.FindById(id);
            if (reservation == null)
            {
                Message = $"Reservation does not exist";
                _logger.LogInformation(Message);
                return NotFound();
            }

            await _context.DeleteAsync(reservation);

            return reservation;
        }
    }
}
