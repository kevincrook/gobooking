﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GoBooking.Interfaces
{
    public interface IDataRepository<TEntity>
    {
        IEnumerable<TEntity> FindAll();
        TEntity FindById(long id);
        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity dbEntity, TEntity entity);
        Task<TEntity> DeleteAsync(TEntity entity);
    }
}
