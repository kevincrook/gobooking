﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GoBooking.Migrations
{
    public partial class authAdded1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RoomPrice",
                table: "Room",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "RoomID",
                keyValue: 1,
                column: "RoomPrice",
                value: 290L);

            migrationBuilder.UpdateData(
                table: "Room",
                keyColumn: "RoomID",
                keyValue: 2,
                column: "RoomPrice",
                value: 159L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RoomPrice",
                table: "Room");
        }
    }
}
