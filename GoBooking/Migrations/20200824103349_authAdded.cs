﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GoBooking.Migrations
{
    public partial class authAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Reservations",
                columns: table => new
                {
                    BookingID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoomID = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    DateBookedFrom = table.Column<DateTime>(nullable: false),
                    DateBookedTo = table.Column<DateTime>(nullable: false),
                    Occupants = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservations", x => x.BookingID);
                });

            migrationBuilder.CreateTable(
                name: "Room",
                columns: table => new
                {
                    RoomID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoomTitle = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Capacity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room", x => x.RoomID);
                });

            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    ImageID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoomId = table.Column<int>(nullable: false),
                    FileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.ImageID);
                    table.ForeignKey(
                        name: "FK_Image_Room_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "RoomID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Room",
                columns: new[] { "RoomID", "Address", "Capacity", "Description", "RoomTitle" },
                values: new object[] { 1, "21 Ocean st Marrochydore", 5, "Well lite room with ocean views", "Lavender Room" });

            migrationBuilder.InsertData(
                table: "Room",
                columns: new[] { "RoomID", "Address", "Capacity", "Description", "RoomTitle" },
                values: new object[] { 2, "21 Ocean st Marrochydore", 15, "Spacious room for all your parliamentary needs", "Oval Room" });

            migrationBuilder.InsertData(
                table: "Image",
                columns: new[] { "ImageID", "FileName", "RoomId" },
                values: new object[] { 1, "lanenderRoom.png", 1 });

            migrationBuilder.InsertData(
                table: "Image",
                columns: new[] { "ImageID", "FileName", "RoomId" },
                values: new object[] { 2, "ovalRomm.png", 2 });

            migrationBuilder.CreateIndex(
                name: "IX_Image_RoomId",
                table: "Image",
                column: "RoomId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Image");

            migrationBuilder.DropTable(
                name: "Reservations");

            migrationBuilder.DropTable(
                name: "Room");
        }
    }
}
