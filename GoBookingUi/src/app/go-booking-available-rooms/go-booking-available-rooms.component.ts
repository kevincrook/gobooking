import { Component, OnInit, Input, Inject } from '@angular/core';
import {SharedServicesService, Room} from '../SharedServices/shared-services.service';
@Component({

  selector: 'app-go-booking-available-rooms',
  templateUrl: './go-booking-available-rooms.component.html',
  styleUrls: ['./go-booking-available-rooms.component.scss']
})


export class GoBookingAvailableRoomsComponent implements OnInit {

selectedRoom: string;
  data: any;
  isLoadingResults = true;
  HighlightRow: Number; 

  Rooms = []
  ClickedRow: any;

  constructor(private api: SharedServicesService) {
  }

  ngOnInit(): void {
    this.api.getRooms(new Room())
    .subscribe(res => {
      this.Rooms.push(res);
      console.log(this.Rooms);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });

    function deleteARoom (id: number){
      this.api.deleteARoom(id);
    }

    function updateARoom (id: number, room: Room){
      this.api.updateARoom(id, room);
    }

    function addARoom (room: Room){
      this.api.addARoom(room);
    }
  }
  deleteARoom (id: number){
    this.api.deleteARoom(id);
  }

  updateARoom (id: number,room: Room){
    this.api.updateARoom(id, room);
  }

  addARoom (room: Room){
    this.api.addARoom(room);
  }
}
