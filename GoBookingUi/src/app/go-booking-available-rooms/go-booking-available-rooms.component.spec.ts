import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoBookingAvailableRoomsComponent } from './go-booking-available-rooms.component';

describe('GoBookingAvailableRoomsComponent', () => {
  let component: GoBookingAvailableRoomsComponent;
  let fixture: ComponentFixture<GoBookingAvailableRoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoBookingAvailableRoomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoBookingAvailableRoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
