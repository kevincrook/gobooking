import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrlRooms = 'https://localhost:44352/api/roomsmanager';
const apiUrlReservations = 'https://localhost:44352/api/ReservationsManager';

export class Room {
 roomID: number;
 roomTitle: string;
 images: string;
 description: string;
 address: string;
 capacity: string;
 roomPrice:number
}

export class Reservation {
  bookingID: number;
  roomID: number;
  email: string;
  dateTo: Date;
  dateFrom: Date;
  occupants: string;
 }

@Injectable({
  providedIn: 'root'
})

export class SharedServicesService {

  constructor(private http: HttpClient) { }
room = Room;

  getRooms (room: Room): Observable<Room[]> {
     return this.http.get<Room[]>(apiUrlRooms);
  }

  getRoom(roomID: number): Observable<Room> {
    const url = `${apiUrlRooms}/${roomID}`;
    return this.http.get<Room>(url);
  }
  
  addARoom (room: Room): Observable<Room> {
    return this.http.post<Room>(apiUrlRooms, room, httpOptions);
  }
  
  updateARoom (id: number, room: Room): Observable<any> {
    const url = `${apiUrlRooms}/${id}`;
    return this.http.put(url, room, httpOptions);
  }
  
  deleteARoom (id: number): Observable<Room> {
    const url = `${apiUrlRooms}/${id}`;
    return this.http.delete<Room>(url, httpOptions);
  }
  
  getReservations (res: Reservation): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(apiUrlReservations);
 }
 
 getReservation(bookingID: number): Observable<Reservation> {
   const url = `${apiUrlReservations}/${bookingID}`;
   return this.http.get<Reservation>(url);
 }
 
 addAReservation (res: Reservation): Observable<Reservation> {
   return this.http.post<Reservation>(apiUrlReservations, res, httpOptions);
 }
 
 updateAReservation (id: number, reservation: Reservation): Observable<any> {
   const url = `${apiUrlReservations}/${id}`;
   return this.http.put(url, reservation, httpOptions);
 }
 
 deleteAReservation (id: number): Observable<Reservation> {
   const url = `${apiUrlReservations}/${id}`;
   return this.http.delete<Reservation>(url, httpOptions);
 }
}

