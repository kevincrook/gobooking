import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GoBookingReservationsComponent } from './go-booking-reservations/go-booking-reservations.component';
import {SharedServicesService} from './SharedServices/shared-services.service';
import { GoBookingAvailableRoomsComponent } from './go-booking-available-rooms/go-booking-available-rooms.component';
import { FormsModule } from '@angular/forms';
import {ReservationFormComponent} from './modals/reservation-form/reservation-form.component';

// Material UI Modules
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    GoBookingReservationsComponent,
    GoBookingAvailableRoomsComponent,
    ReservationFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
  ],

  providers: [
    SharedServicesService
  ],
  bootstrap: [
    AppComponent
  ],
})
export class AppModule { }
