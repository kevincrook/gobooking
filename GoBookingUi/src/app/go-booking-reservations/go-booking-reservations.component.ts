import { Component, OnInit, Input } from '@angular/core';
import {SharedServicesService, Reservation} from '../SharedServices/shared-services.service';

@Component({
  selector: 'app-go-booking-reservations',
  templateUrl: './go-booking-reservations.component.html',
  styleUrls: ['./go-booking-reservations.component.scss']
})
export class GoBookingReservationsComponent implements OnInit {
  isLoadingResults = true;

  Reservations = []

  constructor(private api: SharedServicesService) { }

  ngOnInit(): void {
    this.api.getReservations(new Reservation())
    .subscribe(res => {
      this.Reservations.push(res);
      console.log(this.Reservations);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });

    function deleteAReservation (id: number){
      this.api.deleteAReservation(id);
    }

    function updateARoom (id: number, res: Reservation){
      this.api.updateARoom(id, res);
    }

    function addARoom (res: Reservation){
      this.api.addARoom(res);
    }
  }

  deleteARoom (id: number){
    this.api.deleteARoom(id);
  }

  updateARoom (id: number, res: Reservation){
    this.api.updateAReservation(id, res);
  }

  addARoom (res: Reservation){
    this.api.addAReservation(res);
  }
}

