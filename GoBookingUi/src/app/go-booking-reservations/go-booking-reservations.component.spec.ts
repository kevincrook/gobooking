import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoBookingReservationsComponent } from './go-booking-reservations.component';

describe('GoBookingReservationsComponent', () => {
  let component: GoBookingReservationsComponent;
  let fixture: ComponentFixture<GoBookingReservationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoBookingReservationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoBookingReservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
