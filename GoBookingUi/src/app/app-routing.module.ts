import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoBookingAvailableRoomsComponent } from './go-booking-available-rooms/go-booking-available-rooms.component';
import { GoBookingReservationsComponent } from './go-booking-reservations/go-booking-reservations.component';
import {ReservationFormComponent} from './modals/reservation-form/reservation-form.component';

const routes: Routes = [
  { path: '', component: GoBookingAvailableRoomsComponent, pathMatch: 'full' },
  { path: 'reservations', component: GoBookingReservationsComponent },
  { path: 'reservationform', component: ReservationFormComponent },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
