﻿using GoBooking.Controllers;
using GoBooking.Interfaces;
using GoBooking.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GoBookingTests
{
    public class ReservationsTests
    {
        [Fact]
        public void ShouldReturnsAViewResult_WithAListOfReservations()
        {
            // Arrange
            var mockRepo = new Mock<IDataRepository<Reservation>>();
            mockRepo.Setup(repo => repo.FindAll())
                .Returns(GetTestRooms());
            var controller = new ReservationsManagerController(null, mockRepo.Object);

            // Act
            var result = controller.GetReservations();

            // Assert
            var viewResult = Assert.IsType<List<Reservation>>(result);
            var model = Assert.IsAssignableFrom<List<Reservation>>(
                viewResult);
            Assert.Equal(2, model.Count);
        }

        [Fact]
        public void ShouldNotBookReservationDatesOverLap()
        {
            // Arrange
            var mockRepo = new Mock<IDataRepository<Reservation>>();
            mockRepo.Setup(repo => repo.AddAsync(new Reservation()
            {
                RoomID = 1,
                DateBookedFrom = new DateTime(2020, 08, 21),
                DateBookedTo = new DateTime(2020, 08, 25)

            }))
                .Returns(Task.Run(() => GetTestRooms().FirstOrDefault()));
            var controller = new ReservationsManagerController(null, mockRepo.Object);

            // Act
            var result = controller.PostReservation(GetTestRooms().FirstOrDefault());

            // Assert
            var viewResult = Assert.IsType<Task<ActionResult<Reservation>>>(result);
            var model = Assert.IsAssignableFrom<Task<ActionResult<Reservation>>>(
                viewResult);
            Assert.Null(model.Result.Value);
        }

        private IEnumerable<Reservation> GetTestRooms()
        {
            return new List<Reservation>()
        {
        new Reservation()
        {
            RoomID = 2,
            DateBookedFrom = new DateTime(2020,08,10),
            DateBookedTo = new DateTime(2020,08,15)
            
        },
        new Reservation()
        {
            RoomID = 1,
            
        }
         };
        }
    }
}
