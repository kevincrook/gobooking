using Castle.Core.Logging;
using Castle.DynamicProxy.Generators;
using GoBooking.Controllers;
using GoBooking.Interfaces;
using GoBooking.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xunit;

namespace GoBookingTests
{
    public class RoomBookingTests
    {

        [Fact]
        public void ShouldReturnsAViewResult_WithAListOfRooms()
        {
            // Arrange
            var mockRepo = new Mock<IDataRepository<Room>>();
            mockRepo.Setup(repo => repo.FindAll())
                .Returns(GetTesRooms());
            var controller = new RoomsManagerController(null, mockRepo.Object);

            // Act
            var result = controller.GetAllRooms();

            // Assert
            var viewResult = Assert.IsType<List<Room>>(result);
            var model = Assert.IsAssignableFrom<List<Room>>(
                viewResult);
            Assert.Equal(2, model.Count);
        }

        [Fact]
        public void ShouldReturnsAViewResult_WithASinglefRoom()
        {
            // Arrange
            var mockRepo = new Mock<IDataRepository<Room>>();
            mockRepo.Setup(repo => repo.FindById(1))
                .Returns(GetASingleRoom());
            var controller = new RoomsManagerController(null, mockRepo.Object);

            // Act
            var result = controller.GetRoomByID(1);

            // Assert
            var viewResult = Assert.IsType<Task<ActionResult<Room>>>(result);
            var model = Assert.IsAssignableFrom<Task<ActionResult<Room>>>(
                viewResult);
            Assert.Equal(1, model.Result.Value.RoomID);
        }

        [Fact]
        public void ShouldReturnNoAViewResult_WithDeletingARoom()
        {
            using var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
            var logger = loggerFactory.CreateLogger<Room>();

            var emptyRoomObject = Task.Run(() => GetASingleRoom());
            // Arrange
            var mockRepo = new Mock<IDataRepository<Room>>();
            mockRepo.Setup(repo => repo.DeleteAsync(GetASingleRoom()))
                .Returns(emptyRoomObject);
            var controller = new RoomsManagerController(logger, mockRepo.Object);

            // Act
            var result = controller.GetRoomByID(1);

            // Assert
            var viewResult = Assert.IsType<Task<ActionResult<Room>>>(result);
            var model = Assert.IsAssignableFrom<Task<ActionResult<Room>>>(
                viewResult);
            Assert.Null(model.Result.Value);
        }

        //Room objects created to use for tests.

        private IEnumerable<Room> GetTesRooms()
        {
            return new List<Room>()
        {
        new Room()
        {
            RoomID = 1,
            RoomTitle = "testing Room 1"
        },
        new Room()
        {
            RoomID = 2,
            RoomTitle = "testing Room 2"
        }
         };
        }

        private Room GetASingleRoom()
        {
            return new Room()
            {
                RoomID = 1,
                RoomTitle = "testing Room 1"
            };
        }

        private IEnumerable<Room> GetTestRoom()
        {
            return new List<Room>()
        {
        new Room()
        {
            RoomID = 1,
            RoomTitle = "testing Room 1"
        }
         };
        }
    }
}
